# About

Comparing hashes is an essential operation in digital security, but tedious and error-prone when the hashes are in the form of hexadecimal strings. This repository stores a new hash visualization software with animations. The software, called hashify, uses 4 characters and 4 SVG icons to generate 2-second animations that transmit around 48 bits of a sequence derived from the original hash. It was implemented as a JavaScript library and embedded in a Firefox extension prototype, which uses the library to display a stamp of the HTTPS certificate used on a web page.

The code was developed by Jorge Miguel Ribeiro <jorgemiguelribeiro92@gmail.com> under the supervision of Professor José Coelho de Pina <coelho@ime.usp.br> during the writing of Jorge's Capstone Project in the Computer Science course at University of São Paulo, Brazil. The monograph, resulting from the project, is available, in Portuguese, in the folder **monograph/**.

# License

hashify is MIT-licensed. Please refer to the **src/LICENSE** file for detailed information.

# Demonstration

To see hashify in action, access https://jorgemiguelribeiro92.gitlab.io/hashify/src/html/demo.html 

# How to install and use

The code, available at the folder **src/** was developed as a JavaScript library. To clone this folder is enough to have it available at your system.

In particular, the Firefox extension is available at the folder **src/extension**. To use it you can clone this folder or download the file **hashify_extension.zip** available at the root of this repository.

After copying the extension to your system (via clone of the folder or via download of the .zip), the process of installation is:

- Open Firefox and access the URL: **about:debugging**
- Click on "This Firefox" at the left side
- Click on "Load Temporary Add-on"
- Select the file **manifest.json** inside the **extension** folder (The hashify icon ![alt text](hashify-48.png "hashify icon") will appear on the extensions toolbar)
- Access any website with HTTPS and click on the hashify icon to see the visual stamp

The installation process is explained at this video (in Portuguese): https://youtu.be/raFkfRRXBM0

# Compatibility

hashify was tested on the following operating systems and browsers:

- macOS Catalina 10.15.6 and Firefox 79.0 (64-bits)
- Debian buster 10.4 and Firefox 68.10.0esr (64-bits)

# Contact

Suggestions or questions? Please, write to Jorge Miguel Ribeiro <jorgemiguelribeiro92@gmail.com>
