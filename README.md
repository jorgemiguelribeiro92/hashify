# hashify.js

![alt text](new_comic_hashify.jpg "animation")

This library creates an animated stamp with SVG from hashes that transmit ~48 bits of information.

The stamp is derived entirely from a hash and salt, through iterations of SHA256.

A demonstration is available here: https://jorgemiguelribeiro92.gitlab.io/hashify/src/html/demo.html

For more information, please refer to **doc/** folder.

# License

hashify is MIT-licensed. Please refer to the **src/LICENSE** file for detailed information.
